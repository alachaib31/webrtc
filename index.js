const express = require("express");
const app = express();
app.use(express.static("public"));

const server = app.listen(3000,()=>{
    console.log("listening on ",3000);
})

const io = require("socket.io")(server);

io.on("connection",socket =>{
    console.log(`a user connected`);
    socket.on("create or join",room=>{
        console.log("create or join to room",room);
        console.log( io.sockets.adapter.rooms.get(room));
        const myRoom = io.sockets.adapter.rooms.get(room) || {size: 0}
        const numClients = myRoom.size
        console.log(room,'has',numClients,'clients');
        if(numClients == 0){
            socket.join(room);
            socket.emit("created",room);
        }else if(numClients == 1){
            socket.join(room);
            socket.emit("joined",room)
        }else{
            socket.emit("full",room)
        }
    })
    socket.on("ready",room=>{
        socket.broadcast.to(room).emit("ready");
    })
    socket.on("candidate",event=>{
        console.log(event);
        socket.broadcast.to(event.room).emit("candidate",event);
    })
    socket.on("offer",event=>{
        socket.broadcast.to(event.room).emit("offer",event.sdp);
    })
    socket.on("answer",event=>{
        socket.broadcast.to(event.room).emit("answer",event.sdp);
    })
});