const express = require("express");
const http = require("http");
const path = require("path")
const { Server } = require("socket.io");

const app = express();
const server = http.createServer(app);
const io = new Server(server);

// Serve the static files
app.use(express.static("public"));
app.get("/exe",(req,res)=>{
    console.log("hi")
    res.sendFile(path.join(__dirname,"/public/exemple2.html"))
})
io.on("connection", (socket) => {
  console.log(`User ${socket.id} connected`);
  socket.on("join",(username)=>{
    socket.join(username);
  })
  // Offer event
  socket.on("offer", (offer, id) => {
    console.log(`User ${socket.id} sent an offer to user ${id}`);
    socket.to(id).emit("offer", offer, socket.id);
  });

  // Answer event
  socket.on("answer", (answer, id) => {
    console.log(`User ${socket.id} sent an answer to user ${id}`);
    socket.to(id).emit("answer", answer, socket.id);
  });

  // ICE candidate event
  socket.on("ice-candidate", (candidate, id) => {
    console.log(candidate);
    console.log(`User ${socket.id} sent an ICE candidate to user ${id}`);
    socket.to(id).emit("ice-candidate", candidate, socket.id);
  });

  // Disconnection event
  socket.on("disconnect", () => {
    console.log(`User ${socket.id} disconnected`);
  });
});

server.listen(3000, () => {
  console.log("Server is listening on port 3000");
});
